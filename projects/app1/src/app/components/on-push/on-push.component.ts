import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-on-push',
  templateUrl: './on-push.component.html',
  styleUrls: ['./on-push.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class OnPushComponent implements OnInit {

  /**
   * Valor que seva a incrementar cada segundo en el TS
   * y que dependiendo de la estrategia que siga este componente de
   * detección de cambios, veremos o no, los cambios en la vista HTML
   */
  segundos = 0;

  constructor() { }

  ngOnInit(): void {

    setInterval(() => {
      this.segundos++;
      // Comprobamos el cambio en el TS:
      console.log(`Número de segundos transcurridos: ${this.segundos}`);
    }, 1000)

  }

}
