import { ChangeDetectorRef, Component, Injectable, OnInit } from '@angular/core';
// Importamos MOCKJS para generar nombres aleatorios constantemente
import * as Mock from 'mockjs';


@Injectable({
  providedIn: 'root'
})
export class DataListProvider {

  // Método get para que constantemente nos devuelva 5 nombres aleatorios
  get data() {
    const RandomName = Mock.Random;
    return [
      RandomName.first(),
      RandomName.first(),
      RandomName.first(),
      RandomName.cfirst(),
    ]
  }

}


@Component({
  selector: 'app-detach',
  templateUrl: './detach.component.html',
  styleUrls: ['./detach.component.css']
})
export class DetachComponent implements OnInit {

  constructor(private _ref: ChangeDetectorRef, public dataListProvider: DataListProvider) { }

  ngOnInit(): void {


    // Desacoplamos el TS del HTML a través del método DETACH
    this._ref.detach()

    /**
     * * Cuando un componente está DESACOPLADO, solo hay DOS formas de decirle a Angular
     * que replique los cambios en el HTML
     *
     * ? 1. detectChanges() --> Detecta los cambios en ese momento y los manda al HTML a actualizar
     * ? 2. reattach() --> (mostrado en otro ejemplo) sirve para volver a acolplar de nuevo el componente TS y HTML
     */

    // Cada Tres segundos, le decimos al Angular que revise los nuevos nombres
    setInterval(() => {
      this._ref.detectChanges(); // Acople puntual, cada 3 segundos para actualizar la vista
    }, 3000)

  }

}
