import { ChangeDetectorRef, Component, Injectable, OnInit } from '@angular/core';


// Clase provider que nos va a dar un precio nuevo de Bitcoin cada medio segundo
@Injectable({
  providedIn: 'root'
})
export class PrecioBitcoinProvider {

  precio = 100;

  constructor() {
    setInterval(() => {
      this.precio = Math.floor(Math.random() * 1000) + 100;
      console.log(`Nuevo precio del Bitcoin: ${this.precio}$`);
    }, 500)
  }

}



@Component({
  selector: 'app-reattach',
  templateUrl: './reattach.component.html',
  styleUrls: ['./reattach.component.css'],
  inputs: ['enVivo']
})
export class ReattachComponent implements OnInit {

  mostrarEnVivo: boolean = true;

  constructor(private _ref: ChangeDetectorRef, public precioBitcoinProvider: PrecioBitcoinProvider) { }

  set enVivo(valor: boolean) {
    this.mostrarEnVivo = valor;

    if (valor) {
      this._ref.reattach();
    } else {
      this._ref.detach();
    }

  }



  ngOnInit(): void {
  }

}
