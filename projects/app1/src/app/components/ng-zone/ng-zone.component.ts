import { Component, NgZone, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-zone',
  templateUrl: './ng-zone.component.html',
  styleUrls: ['./ng-zone.component.css']
})
export class NgZoneComponent implements OnInit {

  progreso = 0; // 0% - 100%
  texto = '';


  constructor( private _ngZone: NgZone) { }

  ngOnInit(): void {
  }

  incrementarProgreso( terminar: () => void ) {

    this.progreso += 1;
    console.log(`Progreso actual: ${this.progreso}%`);

    if (this.progreso < 100) {

      window.setTimeout(() => {
        this.incrementarProgreso(terminar);
      }, 10)

    } else {
      terminar();
    }

  }

  // * Método que realiza el incremento DENTRO de angular Zone
  // Por defecto, siempre Ejecutamos dentro de Angular Zone
  aumentarDentroNgZone() {
    this.texto = 'DENTRO';
    this.progreso = 0; // lo resetamos
    this.incrementarProgreso(() => console.log('Dentro de angular Zone: Aumento realizado'));
  }

  // * Método que realiza el incremento FUERA de angular Zone
  aumentarFueraNgZone() {
    this.texto = 'FUERA';
    this.progreso = 0; // lo resetamos

    // EJECUTAMOS FUERA DE ANGULAR ZONE
    this._ngZone.runOutsideAngular(() => {
      this.incrementarProgreso(() => {
        // Cuando termine de incrementar el progreso a 100
        // Es cuando pasamos a ejecutar en Angular Zone
        // Volvemos a reacoplar los cambios del componente
        this._ngZone.run(() => console.log('Fuera de angular Zone: Aumento realizado'))
      });
    });
  }
}
