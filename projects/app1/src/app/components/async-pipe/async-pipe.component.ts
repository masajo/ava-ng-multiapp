import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-async-pipe',
  templateUrl: './async-pipe.component.html',
  styleUrls: ['./async-pipe.component.css']
})
export class AsyncPipeComponent implements OnInit {

  @Input() items$ !: Observable<any>;

  // Lista de números que es actualizada con cada next() del observable
  listaItems: [] = [];

  constructor() { }

  ngOnInit(): void {
    this.items$.subscribe(
      (lista) => {
        this.listaItems = lista;
      },
      (error) => console.log(`Ha ocurrido un error al obtener la lista de items: ${error}`),
      () => console.log('Nuevos items recibidos')
    );
  }

}
