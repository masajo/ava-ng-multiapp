import { ModuleWithProviders, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { SecondPageComponent } from './pages/second-page/second-page.component';
import { NavComponent } from './components/nav/nav.component';
import { AsyncPipeComponent } from './components/async-pipe/async-pipe.component';
import { NgZoneComponent } from './components/ng-zone/ng-zone.component';
import { OnPushComponent } from './components/on-push/on-push.component';
import { DetachComponent } from './components/detach/detach.component';
import { ReattachComponent } from './components/reattach/reattach.component';

// Generamos una lista de providers para el módulo principal y el SharedModule
const providers: [] = [];

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    SecondPageComponent,
    NavComponent,
    AsyncPipeComponent,
    NgZoneComponent,
    OnPushComponent,
    DetachComponent,
    ReattachComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers, // providers: providers
  bootstrap: [AppComponent]
})
export class AppModule { }


// Creamos un módulo compartido que se usa en el
// AppRoutingModule de App0 para poder cargar sus rutas, providers y sus componentes
@NgModule({})
export class App1SharedModule {

  static forRoot(): ModuleWithProviders<any> {
    return {
      ngModule: AppModule,
      providers // providers: providers
    }
  }

}
