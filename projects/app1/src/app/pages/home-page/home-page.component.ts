import { Component, OnInit } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  live: boolean = true;

  items = [{}]
  items$ = new BehaviorSubject(this.items);


  constructor() { }

  ngOnInit(): void {
  }

  addItem() {

    const nuevoItem = Math.floor(Math.random() * 100) + 1;

    this.items.push(
      {
        numero: nuevoItem
      }
    );

    this.items$.next(this.items);

  }

}
