import { ModuleWithProviders, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { SecondPageComponent } from './pages/second-page/second-page.component';
import { NavComponent } from './components/nav/nav.component';

const providers: [] = [];

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    SecondPageComponent,
    NavComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

// Creamos un módulo compartido que se usa en el
// AppRoutingModule de App0 para poder cargar sus rutas, providers y sus componentes
@NgModule({})
export class App2SharedModule {

  static forRoot(): ModuleWithProviders<any> {
    return {
      ngModule: AppModule,
      providers // providers: providers
    }
  }

}
